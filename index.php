<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new animal("Shaun");
echo "Name = " .$sheep->name. "<br>";
echo "Legs = " .$sheep->legs. "<br>";
echo "Cold blooded = " .$sheep->cold_blooded. "<br><br>";

$frog = new frog("Buduk");
echo "Name = " .$frog->name. "<br>";
echo "Legs = " .$frog->legs. "<br>";
echo "Cold blooded = " .$frog->cold_blooded. "<br>";
echo "Jump = " .$frog->jump. "<br><br>";

$ape = new ape("kera sakti");
echo "Name = " .$ape->name. "<br>";
echo "Legs = " .$ape->legs. "<br>";
echo "Cold blooded = " .$ape->cold_blooded. "<br>";
echo "Yell = " .$ape->yell. "<br><br>"

?>